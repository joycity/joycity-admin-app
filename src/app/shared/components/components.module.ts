import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderSimpleComponent} from './header-simple/header-simple.component';
import {IonicModule} from '@ionic/angular';


@NgModule({
  declarations: [
    HeaderSimpleComponent
  ],
  exports: [
    HeaderSimpleComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class ComponentsModule {
}
