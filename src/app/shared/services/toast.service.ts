import {Injectable} from '@angular/core';
import {ToastController, ToastOptions} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(public toastController: ToastController) {
  }

  async success(toastOptions: ToastOptions) {
    const toast = await this.toastController.create({
      header: toastOptions.header,
      message: toastOptions.message,
      icon: toastOptions.icon,
      position: toastOptions.position,
      color: 'success',
      animated: true,
      mode: 'ios',
      duration: toastOptions.duration,
      buttons: toastOptions.buttons,
      translucent: toastOptions.translucent,
    });
    await toast.present();

    const { role } = await toast.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
}
