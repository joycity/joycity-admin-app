import { Injectable } from '@angular/core';
import {ActionSheetController, ActionSheetOptions} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ActionSheetService {
  public x: '';

  constructor(private actionSheetController: ActionSheetController) { }

  public async create(actionSheetOptions: ActionSheetOptions) {
    const actionSheet = await this.actionSheetController.create({
      header: actionSheetOptions.header,
      subHeader: actionSheetOptions.subHeader,
      cssClass: 'my-custom-class',
      buttons: actionSheetOptions.buttons,
      animated: true,
      translucent: true,
      mode: 'ios',
      backdropDismiss: actionSheetOptions.backdropDismiss
    });
    await actionSheet.present();
    return actionSheet;
  }
}
