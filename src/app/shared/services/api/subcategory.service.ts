import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubcategoryService {
  constructor(private http: HttpClient) {
  }

  public addSubcategory(data: AddSubcategoryRequestBodyModel): Observable<{ result: AddSubcategoryResponseModel }> {
    return this.http.post<{ result: AddSubcategoryResponseModel }>('api/subcategory/add', data);
  }
}

export class AddSubcategoryRequestBodyModel {
  public title?: string;
  public categoryId?: string;
}

export class AddSubcategoryResponseModel {
  public id?: string;
  public title?: string;
  public category?: string;
}
