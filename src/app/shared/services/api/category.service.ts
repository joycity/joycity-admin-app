import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(private http: HttpClient) {
  }

  public getAll(): Observable<{ result: CategoryModel[] }> {
    return this.http.get<{ result: CategoryModel[] }>('api/category/get-all');
  }

  public addCategory(data: AddCategoryRequestBodyModel): Observable<{ result: AddCategoryResponseModel }> {
    return this.http.post<{ result: AddCategoryResponseModel }>('api/category/add', data);
  }

  public removeCategory(id: string): Observable<{ result: RemoveCategoryResponseModel }> {
    return this.http.post<{ result: RemoveCategoryResponseModel }>('api/category/remove', {id});
  }
}

export class CategoryModel {
  public id?: string;
  public title?: string;
  public subcategories?: SubcategoryModel[];
}

export class SubcategoryModel {
  public id?: string;
  public title?: string;
  public category?: string;
}

export class AddCategoryRequestBodyModel {
  public title?: string;
}

export class AddCategoryResponseModel {
  public id?: string;
  public title?: string;
  public subcategories?: SubcategoryModel[];
}

export class RemoveCategoryResponseModel {
  public id?: string;
  public title?: string;
  public subcategories?: SubcategoryModel[];
}
