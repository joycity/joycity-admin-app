import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  constructor(private http: HttpClient) {
  }

  public getAll() {
    return this.http.get('api/products/get-all');
  }

  public addProduct(body: AddProductRequestBodyModel) {
    return this.http.post<{result: AddProductResponseModel}>('api/products/add', body);
  }
}

export class AddProductRequestBodyModel {
  public title: string;
  public categoryId: string;
  public subcategoryId: string;
  public price: number;
  public discount?: number;
  public inventory?: number;
  public images?: string[];
  public tags?: number[];
}

export class AddProductResponseModel {
  public id: string;
  public title: string;
  public categoryId: string;
  public subcategoryId: string;
  public price: number;
  public discount?: number;
  public inventory?: number;
  public images?: string[];
  public tags?: number[];
}
