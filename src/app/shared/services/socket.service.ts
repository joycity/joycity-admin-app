import {Injectable} from '@angular/core';
import {io} from 'socket.io-client';
import {Socket} from 'socket.io-client/build/esm/socket';
import {Router} from '@angular/router';
import {ToastService} from './toast.service';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  public instance: Socket;

  constructor(private router: Router,
              private toastService: ToastService) {
    this.instance = io(environment.apiUrl, {
      withCredentials: true,
    });

    this.instance.on('order', async (data) => {
      if (data.success) {
        if (router.url !== ['', 'tabs', 'orders'].join('/')) {
          await toastService.success({
            duration: 1000,
            message: 'سفارش جدید',
            header: undefined,
            mode: 'ios',
            position: 'top',
            animated: true
          });
        }
      }
    });
  }
}
