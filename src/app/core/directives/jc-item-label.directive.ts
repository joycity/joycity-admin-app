import {Directive, ElementRef} from '@angular/core';
import {settings} from '../../configs/settings';

@Directive({
  selector: '[appJcItemLabel]'
})
export class JcItemLabelDirective {
  constructor(private el: ElementRef) {
    this.el.nativeElement.position = settings.labelsPositioningType;
  }
}
