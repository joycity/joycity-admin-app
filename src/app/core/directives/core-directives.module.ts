import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {JcItemDirective} from './jc-item.directive';
import {JcItemLabelDirective} from './jc-item-label.directive';
import {JcItemInputDirective} from './jc-item-input.directive';


@NgModule({
  declarations: [
    JcItemDirective,
    JcItemLabelDirective,
    JcItemInputDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    JcItemDirective,
    JcItemLabelDirective,
    JcItemInputDirective
  ]
})
export class CoreDirectivesModule {
}
