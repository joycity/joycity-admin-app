import {Directive, ElementRef} from '@angular/core';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'ion-item'
})
export class JcItemDirective {
  constructor(private el: ElementRef) {
    this.el.nativeElement.dir = 'rtl';
    this.el.nativeElement.lines = 'full';
    this.el.nativeElement.className = 'item-transparent';
  }
}
