import {Directive, ElementRef} from '@angular/core';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'ion-input'
})
export class JcItemInputDirective {
  constructor(private el: ElementRef) {
    this.el.nativeElement.className = 'text-end';
  }
}
