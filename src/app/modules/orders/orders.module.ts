import {IonicModule} from '@ionic/angular';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {OrdersPage} from './orders-page.component';
import {OrdersPageRoutingModule} from './orders-routing.module';
import {RouterModule} from '@angular/router';
import {ComponentsModule} from '../../shared/components/components.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{path: '', component: OrdersPage}]),
    OrdersPageRoutingModule,
    ComponentsModule
  ],
  declarations: [OrdersPage]
})
export class OrdersPageModule {
}
