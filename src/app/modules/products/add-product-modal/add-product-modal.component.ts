import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {CategoryModel, SubcategoryModel} from '../../../shared/services/api/category.service';
import {ToastService} from '../../../shared/services/toast.service';
import {ProductsService} from '../../../shared/services/api/products.service';

@Component({
  selector: 'app-add-product-modal',
  templateUrl: './add-product-modal.component.html',
  styleUrls: ['./add-product-modal.component.scss'],
})
export class AddProductModalComponent implements OnInit {
  @Input() category: CategoryModel;
  @Input() subcategory: SubcategoryModel;
  public productTitle = '';
  public discount;
  public price;
  public inventory;
  public discounts: number[] = [];

  constructor(private modalController: ModalController,
              private productService: ProductsService,
              private toastService: ToastService) { }

  ngOnInit() {
    for (let i = 0; i <= 100; i++) {
      this.discounts.push(i);
    }
  }

  public closeModal(callback = () => {
  }) {
    this.modalController.getTop().then((res) => {
      res.dismiss().then(() => {
        callback();
      });
    });
  }

  public addProduct() {
    this.productService.addProduct({
      title: this.productTitle,
      categoryId: this.category.id,
      subcategoryId: this.subcategory.id,
      price: this.price,
      inventory: this.inventory,
      discount: this.discount,
      images: []
    }).subscribe((res) => {
      this.closeModal(async () => {
        await this.toastService.success({
          header: 'موفق',
          message: `نام: ${res.result.title}`,
          icon: 'checkmark-outline',
          duration: 5000,
          position: 'bottom',
          translucent: true
        });
      });
    });
  }
}
