import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {CategoryModel} from '../../shared/services/api/category.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsDataPassingService {
  public reFetchCategories: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public categories: CategoryModel[];

  constructor() { }
}
