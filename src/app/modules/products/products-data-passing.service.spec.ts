import { TestBed } from '@angular/core/testing';

import { ProductsDataPassingService } from './products-data-passing.service';

describe('ProductsDataPassingService', () => {
  let service: ProductsDataPassingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductsDataPassingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
