import {Component, OnInit} from '@angular/core';
import {CategoryModel, CategoryService} from '../../shared/services/api/category.service';
import {IonRouterOutlet, ModalController} from '@ionic/angular';
import {AddCategoryModalComponent} from './add-category-modal/add-category-modal.component';
import {ProductsDataPassingService} from './products-data-passing.service';
import {ActionSheetService} from '../../shared/services/action-sheet.service';
import {ToastService} from '../../shared/services/toast.service';
import {SelectCategoryModalComponent} from './select-category-modal/select-category-modal.component';

@Component({
  selector: 'app-products',
  templateUrl: 'products-page.component.html',
  styleUrls: ['products-page.component.scss']
})
export class ProductsPage implements OnInit {
  public categories: CategoryModel[];

  constructor(private modalController: ModalController,
              private routerOutlet: IonRouterOutlet,
              private actionSheetService: ActionSheetService,
              private toastService: ToastService,
              private productsDataPassingService: ProductsDataPassingService,
              private categoryService: CategoryService) {
    productsDataPassingService.reFetchCategories.asObservable().subscribe((res) => {
      if (res) {
        this.getCategories();
        productsDataPassingService.reFetchCategories.next(false);
      }
    });
  }

  ngOnInit() {
    this.getCategories();
  }

  public hide() {
  }

  public async delete(cat: CategoryModel) {
    let actionSheet = await this.actionSheetService.create({
      header: `آیا میخواهید ${cat.title} حذف شود؟`,
      buttons: [
        {
          role: 'destructive',
          text: 'حذف (۲)',
          handler: async () => {
            await actionSheet.dismiss();
            actionSheet = await this.actionSheetService.create({
              header: `آیا میخواهید ${cat.title} حذف شود؟`,
              buttons: [
                {
                  role: 'destructive',
                  text: 'حذف (۱)',
                  handler: async () => {
                    await actionSheet.dismiss();
                    actionSheet = await this.actionSheetService.create({
                      header: `آیا میخواهید ${cat.title} حذف شود؟`,
                      buttons: [
                        {
                          role: 'destructive',
                          text: 'حذف',
                          handler: () => {
                            this.categoryService.removeCategory(cat.id).subscribe((res) => {
                              this.productsDataPassingService.reFetchCategories.next(true);
                              this.toastService.success({
                                header: 'موفق',
                                position: 'bottom',
                                message: `${res.result.title} حذف شد.`,
                                icon: 'trash-outline',
                                duration: 5000,
                                translucent: true
                              });
                            });
                          }
                        },
                        {
                          role: 'cancel',
                          text: 'انصراف',
                          handler: () => {
                            actionSheet.dismiss();
                          }
                        }
                      ]
                    });
                  }
                },
                {
                  role: 'cancel',
                  text: 'انصراف',
                  handler: () => {
                    actionSheet.dismiss();
                  }
                }
              ]
            });
          }
        },
        {
          role: 'cancel',
          text: 'انصراف',
          handler: () => {
            actionSheet.dismiss();
          }
        }
      ]
    });
  }

  public async addCategoryModal() {
    const modal = await this.modalController.create({
      component: AddCategoryModalComponent,
      componentProps: {
        category: null,
        adding: 'category',
      },
      mode: 'ios',
      presentingElement: this.routerOutlet.nativeEl,
      animated: true,
      handle: true,
      swipeToClose: true,
    });
    return await modal.present();
  }

  public async selectCategoryModal(addingSubcategory) {
    const modal = await this.modalController.create({
      component: SelectCategoryModalComponent,
      componentProps: {
        categories: this.categories,
        addingSubcategory,
      },
      mode: 'ios',
      presentingElement: this.routerOutlet.nativeEl,
      animated: true,
      handle: true,
      swipeToClose: true,
    });
    return await modal.present();
  }

  public goToCategory() {
  }

  private getCategories() {
    this.categoryService.getAll().subscribe((res) => {
      this.categories = res.result;
      this.productsDataPassingService.categories = res.result;
    });
  }
}
