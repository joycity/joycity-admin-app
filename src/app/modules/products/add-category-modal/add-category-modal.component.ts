import {Component, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {CategoryService} from '../../../shared/services/api/category.service';
import {ToastService} from '../../../shared/services/toast.service';
import {ProductsDataPassingService} from '../products-data-passing.service';

@Component({
  selector: 'app-add-category-modal',
  templateUrl: './add-category-modal.component.html',
  styleUrls: ['./add-category-modal.component.scss'],
})
export class AddCategoryModalComponent implements OnInit {
  public categoryTitle = '';

  constructor(private modalController: ModalController,
              private categoryService: CategoryService,
              private productsDataPassingService: ProductsDataPassingService,
              private toastService: ToastService) {
  }

  ngOnInit() {
  }

  public closeModal(callback = () => {}) {
    this.modalController.getTop().then((res) => {
      res.dismiss().then(() => {
        callback();
      });
    });
  }

  public addCategory() {
    this.categoryService.addCategory({title: this.categoryTitle}).subscribe((res) => {
      this.closeModal(async () => {
        this.productsDataPassingService.reFetchCategories.next(true);
        await this.toastService.success({
          header: 'موفق',
          message: `نام: ${res.result.title}`,
          icon: 'checkmark-outline',
          duration: 5000,
          position: 'bottom',
          translucent: true
        });
      });
    });
  }
}
