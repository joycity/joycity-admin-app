import {IonicModule} from '@ionic/angular';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ProductsPage} from './products-page.component';
import {ProductsPageRoutingModule} from './products-routing.module';
import {RouterModule} from '@angular/router';
import {ComponentsModule} from '../../shared/components/components.module';
import {AddCategoryModalComponent} from './add-category-modal/add-category-modal.component';
import {SelectCategoryModalComponent} from './select-category-modal/select-category-modal.component';
import {AddSubcategoryModalComponent} from './add-subcategory-modal/add-subcategory-modal.component';
import {SelectSubcategoryModalComponent} from './select-subcategory-modal/select-subcategory-modal.component';
import {AddProductModalComponent} from './add-product-modal/add-product-modal.component';
import {CoreDirectivesModule} from '../../core/directives/core-directives.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{path: '', component: ProductsPage}]),
    ProductsPageRoutingModule,
    ComponentsModule,
    CoreDirectivesModule
  ],
  declarations: [
    ProductsPage,
    AddCategoryModalComponent,
    SelectCategoryModalComponent,
    AddSubcategoryModalComponent,
    SelectSubcategoryModalComponent,
    AddProductModalComponent
  ],
  exports: []
})
export class ProductsPageModule {
}
