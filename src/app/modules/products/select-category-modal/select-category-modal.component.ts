import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {CategoryModel} from '../../../shared/services/api/category.service';
import {AddSubcategoryModalComponent} from '../add-subcategory-modal/add-subcategory-modal.component';
import {SelectSubcategoryModalComponent} from '../select-subcategory-modal/select-subcategory-modal.component';

@Component({
  selector: 'app-select-category-modal',
  templateUrl: './select-category-modal.component.html',
  styleUrls: ['./select-category-modal.component.scss'],
})
export class SelectCategoryModalComponent implements OnInit {
  @Input() addingSubcategory: boolean;
  @Input() categories: CategoryModel[];

  constructor(private modalController: ModalController) {
  }

  ngOnInit() {
  }

  public async selectCategory(cat: CategoryModel) {
    const modal = await this.modalController.create({
      component: this.addingSubcategory ? AddSubcategoryModalComponent : SelectSubcategoryModalComponent,
      componentProps: {
        category: cat,
        subcategories: this.addingSubcategory ? undefined : cat.subcategories
      },
      mode: 'ios',
      presentingElement: await this.modalController.getTop(),
      animated: true,
      handle: true,
      swipeToClose: true,
    });
    return await modal.present();
  }

  public closeModal(callback = () => {}) {
    this.modalController.getTop().then((res) => {
      res.dismiss().then(() => {
        callback();
      });
    });
  }

}
