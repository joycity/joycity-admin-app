import {Component, Input, OnInit} from '@angular/core';
import {CategoryModel, SubcategoryModel} from '../../../shared/services/api/category.service';
import {ModalController} from '@ionic/angular';
import {AddProductModalComponent} from '../add-product-modal/add-product-modal.component';

@Component({
  selector: 'app-select-subcategory-modal',
  templateUrl: './select-subcategory-modal.component.html',
  styleUrls: ['./select-subcategory-modal.component.scss'],
})
export class SelectSubcategoryModalComponent implements OnInit {
  @Input() category: CategoryModel;
  @Input() subcategories: SubcategoryModel[];

  constructor(private modalController: ModalController) {
  }

  ngOnInit() {
  }

  public async selectSubcategory(subcategoryModel: SubcategoryModel) {
    const modal = await this.modalController.create({
      component: AddProductModalComponent,
      componentProps: {
        category: this.category,
        subcategory: subcategoryModel
      },
      mode: 'ios',
      presentingElement: await this.modalController.getTop(),
      animated: true,
      handle: true,
      swipeToClose: true,
    });
    return await modal.present();
  }

  public closeModal(callback = () => {}) {
    this.modalController.getTop().then((res) => {
      res.dismiss().then(() => {
        callback();
      });
    });
  }

}
