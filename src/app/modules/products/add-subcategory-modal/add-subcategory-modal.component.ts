import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {CategoryModel} from '../../../shared/services/api/category.service';
import {ToastService} from '../../../shared/services/toast.service';
import {SubcategoryService} from '../../../shared/services/api/subcategory.service';

@Component({
  selector: 'app-add-subcategory-modal',
  templateUrl: './add-subcategory-modal.component.html',
  styleUrls: ['./add-subcategory-modal.component.scss'],
})
export class AddSubcategoryModalComponent implements OnInit {
  @Input() category: CategoryModel;
  public subcategoryTitle = '';

  constructor(private modalController: ModalController,
              private subcategoryService: SubcategoryService,
              private toastService: ToastService) {
  }

  ngOnInit() {
  }

  public closeModal(callback = () => {
  }) {
    this.modalController.getTop().then((res) => {
      res.dismiss().then(() => {
        callback();
      });
    });
  }

  public addSubcategory() {
    this.subcategoryService.addSubcategory({
      title: this.subcategoryTitle,
      categoryId: this.category.id
    }).subscribe((res) => {
      this.closeModal(async () => {
        await this.toastService.success({
          header: 'موفق',
          message: `نام: ${res.result.title}`,
          icon: 'checkmark-outline',
          duration: 5000,
          position: 'bottom',
          translucent: true
        });
      });
    });
  }
}
