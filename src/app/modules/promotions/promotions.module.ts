import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PromotionsPage} from './promotions-page.component';
import {PromotionsPageRoutingModule} from './promotions-routing.module';
import {ComponentsModule} from '../../shared/components/components.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{path: '', component: PromotionsPage}]),
    PromotionsPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [PromotionsPage]
})
export class PromotionsPageModule {
}
